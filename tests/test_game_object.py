import pytest
import unittest
import tempfile
import os
import json
import lib.prefab as Prefab
from mock import Mock, MagicMock, PropertyMock, patch

from lib.game_object import GameObject


class Types:
    Enemies_Monsters = [
        Prefab.ENEMY_BOSS,
        Prefab.ENEMY_TROLL]
    Enemies_Buildings = [
        Prefab.ENEMY_CLIFF_PORTAL,
        Prefab.ENEMY_PORTAL]
    Enemies = Enemies_Monsters + Enemies_Buildings

    Buildings_Upgradeable = [
        Prefab.BUILDING_FARMHOUSE_0,
        Prefab.BUILDING_TOWER_0,
        Prefab.BUILDING_TOWER_1,
        Prefab.BUILDING_TOWER_2,
        Prefab.BUILDING_TOWER_3,
        Prefab.BUILDING_WALL_0,
        Prefab.BUILDING_WALL_2,
        Prefab.BUILDING_WALL_3,
        Prefab.BUILDING_CASTLE_0,
        Prefab.BUILDING_CASTLE_2
    ]

    Buildings = Buildings_Upgradeable

    Characters_Peons = [
        Prefab.CHARACTER_ARCHER,
        Prefab.CHARACTER_BEGGAR,
        Prefab.CHARACTER_KNIGHT,
        Prefab.CHARACTER_PEASANT,
        Prefab.CHARACTER_WORKER
        # TODO: Farmer
    ]

    Characters_Misc = [  # TODO BANKER
        Prefab.CHARACTER_MERCHANT, Prefab.CHARACTER_GHOST]

    Characters_Player = [Prefab.CHARACTER_PLAYER]

    Characters = Characters_Peons + Characters_Player + Characters_Misc

    All = Characters + Buildings + Enemies


class TestConstructor:
    def test_raises_error_if_ctor_arg_is_not_a_dict(self):
        with pytest.raises(TypeError):
            GameObject(Prefab.CHARACTER_PLAYER)

    def test_exposes_input_dict_as_native_properties(self):
        expected_name = 'Phil Collins'
        expected_bands = ['Genesis',
                          'Phil Collins',
                          'Brand X',
                          'Flaming Youth']
        input = {'name': expected_name,
                 'bands': expected_bands}
        sut = GameObject(input)
        assert sut.name == expected_name
        assert sut.bands == expected_bands


class TestProxyDictJsonHandling:
    test_json = """
    {
        "uniqueID": "SampleObject",
        "prefabPath": "Prefab/Test",
        "localPosition": {
            "x": 1.234,
            "y": 2.345,
            "z": 0.123
        }
    }"""

    test_object = json.loads(test_json)

    def test_keeps_nested_dict_values(self):
        sut = GameObject(TestProxyDictJsonHandling.test_object)
        assert hasattr(sut, 'localPosition')
        assert hasattr(sut.localPosition, 'x')

    def test_writes_back_nested_dict_values(self):
        sut = GameObject(TestProxyDictJsonHandling.test_object)
        result = sut.to_json()
        assert 'localPosition' in result
        assert '1.234' in result
        assert '2.345' in result
        assert '0.123' in result


class TestMutators:
    def test_toggles_invincible_to_on(self):
        # sut = GameObject(Prefab.CHARACTER_PLAYER)
        pass

    def test_toggles_invincible_to_off(self):
        # sut = GameObject(Prefab.CHARACTER_PLAYER)
        pass

    def test_sets_coins_in_wallet_for_player(self):
        # sut = GameObject(Prefab.CHARACTER_PLAYER)
        # TODO - move to character-specific gameobject tests file
        pass


class TestCreateFromTemplate:
    @pytest.mark.parametrize("object_type", Types.All)
    def test_creates_correct_type_from_template(self, object_type):
        sut = GameObject.from_template(object_type)
        assert sut.prefabPath == object_type

    @pytest.mark.parametrize("object_type", Types.All)
    def test_created_objects_have_no_parent_object_reference(self, object_type):
        sut = GameObject.from_template(object_type)
        assert sut.parentObject['linkedObjectID'] == ''

    @pytest.mark.parametrize("object_type", Types.All)
    def test_created_objects_have_expected_common_properties(self, object_type):
        sut = GameObject.from_template(object_type)
        assert hasattr(sut, 'uniqueID')
        assert hasattr(sut, 'prefabPath')
        assert hasattr(sut, 'name')
        assert hasattr(sut, 'componentData2')
        assert hasattr(sut, 'localPosition')

    def test_assigns_a_unique_id_to_each_new_instance(self):
        sut1 = GameObject.from_template(Prefab.CHARACTER_PLAYER)
        sut2 = GameObject.from_template(Prefab.CHARACTER_PLAYER)
        assert sut1.uniqueID != sut2.uniqueID
