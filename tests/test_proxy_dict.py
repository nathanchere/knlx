import pytest
import unittest
import tempfile
import os
import json
from mock import Mock, MagicMock, PropertyMock, patch
from lib.proxy_dict import *


class TestProxyDictJsonHandling:
    test_json = """
    {
        "uniqueID": "SampleObject",
        "prefabPath": "Prefab/Test",
        "localPosition": {
            "x": 1.234,
            "y": 2.345,
            "z": 0.123
        }
    }"""

    test_object = json.loads(test_json)
    test_proxy = ProxyDict(test_object)

    def test_can_serialize_proxy_dict(self):
        sut = ProxyDict(TestProxyDictJsonHandling.test_object)

        result = json.dumps(sut, cls=GameObjectJsonEncoder)
        assert '1.234' in result

    def test_converts_nested_dict_to_proxy_dict(self):
        sut = ProxyDict(TestProxyDictJsonHandling.test_object)
        assert isinstance(sut.localPosition, ProxyDict)
