import pytest
import unittest
import tempfile
import os
import json
import lib.prefab as Prefab
from mock import Mock, MagicMock, PropertyMock, patch

from lib.save_file import SaveFile


SAVE_FILES = [
    'tests/testdata/newgame.json',
    # 'tests/testdata/newgame_xmas.json'
]


class TestReading:
    @pytest.mark.parametrize("filename", SAVE_FILES)
    def test_loads_save_file_without_errors(self, filename):
        sut = SaveFile(filename)
        assert sut


class TestWriting:
    pass


class TestEditing:
    pass
