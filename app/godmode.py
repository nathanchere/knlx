from lib.save_file import SaveFile


def do_godmode(args):
    savefile = SaveFile()
    breakpoint()
    player = get_player(savefile)
    player.set_invincible()
    savefile.replace_object(player)
    savefile.write()
