from lib.save_file import SaveFile


def do_spawn(args):
    """
    Spawn some invincible knights
    """
    savefile = SaveFile()
    for typ in [GameObject.CHARACTER_PEASANT,
                GameObject.CHARACTER_ARCHER,
                GameObject.CHARACTER_ARCHER,
                GameObject.CHARACTER_ARCHER,
                GameObject.CHARACTER_ARCHER]:
        char = GameObject.from_template(typ)
        char.set_position(x=0.1)
        char.set_invincible(True)
        savefile.add_object(char)
    savefile.write()
