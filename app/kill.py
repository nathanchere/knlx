import lib.prefab as Prefab
from lib.save_file import SaveFile


def do_kill(args):
    """
    Kill all the monsters
    """
    savefile = SaveFile()
    total_removed = 0

    for enemy_type in [Prefab.ENEMY_TROLL, Prefab.ENEMY_BOSS]:
        removed_count = savefile.remove_object_by_prefab(enemy_type)
        total_removed += removed_count

        if removed_count:
            print(f'-> Killing {removed_count}x {enemy_type}')

    if total_removed:
        print(f'Killed a total of {total_removed} monsters')
        savefile.write()
    else:
        print('No monsters found in savefile')


def do_armageddon(args):
    """
    Kill all the monsters
    """
    savefile = SaveFile()
    total_removed = 0

    for char_type in [Prefab.CHARACTER_ARCHER, Prefab.CHARACTER_BEGGAR,
                      Prefab.CHARACTER_KNIGHT, Prefab.CHARACTER_PEASANT,
                      Prefab.CHARACTER_WORKER]:
        count_before = len(savefile.objects)
        savefile.objects = [
            obj for obj in savefile.objects if not GameObject(obj).is_character(char_type)]
        removed_count = count_before - len(savefile.objects)
        total_removed += removed_count

        if removed_count:
            print(f'-> Killing {removed_count}x {char_type}')

    for enemy_type in [Prefab.ENEMY_TROLL, Prefab.ENEMY_BOSS]:
        count_before = len(savefile.objects)
        savefile.objects = [
            obj for obj in savefile.objects if not GameObject(obj).is_enemy(enemy_type)]
        removed_count = count_before - len(savefile.objects)
        total_removed += removed_count

        if removed_count:
            print(f'-> Killing {removed_count}x {enemy_type}')

    if total_removed:
        print(f'Killed a total of {total_removed} monsters')
        savefile.write()
    else:
        print('No monsters found in savefile')
