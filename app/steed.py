from lib.save_file import SaveFile
import lib.constants as constants
import lib.prefab as Prefab
import logging


def do_steed(args):
    new_prefab = constants.steeds[args.steed]
    savefile = SaveFile()
    player = [x for x in savefile.objects if x.is_player()][0]
    steed = [x for x in savefile.objects if x.is_steed() and
             x.has_parent(player.uniqueID)][0]

    steed.prefabPath = new_prefab
    steed.name = f'{new_prefab}(Clone)'
    # NOTE: do not do this unless you update the reference in the player object too
    # steed.randomise_id()
    savefile.player_preview.steedPrefabPath = new_prefab
    savefile.write()
