from lib.save_file import SaveFile
from datetime import datetime


def do_debug():
    print('No options detected. For help, use: knlx -h')
    savefile = SaveFile()

    object_count = len(savefile.objects)
    monster_count = len(
        [obj for obj in savefile.objects if obj.is_monster()])
    enemy_count = len(
        [obj for obj in savefile.objects if obj.is_enemy()]) - monster_count
    character_count = len(
        [obj for obj in savefile.objects if obj.is_character()])
    building_count = len(
        [obj for obj in savefile.objects if obj.is_building()])
    print('==[ Debug Info ]==')
    print(f'Save file found at {savefile.path}')
    print(f'Game contains {object_count} objects')
    print(f' -> {monster_count} monsters')
    print(f' -> {enemy_count} non-monster enemies')
    print(f' -> {character_count} characters')
    print(f' -> {building_count} buildings')
