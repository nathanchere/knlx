from lib.save_file import SaveFile
from datetime import datetime
from os import rename
from shutil import copyfile


def do_backup(args):
    savefile = SaveFile()

    if args.backup == True:  # default, no filename set
        folder = SaveFile.SAVE_PATH
    else:
        folder = args.backup

    savefile.backup(folder)


def do_restore(args):
    do_backup(args)
    os.rename('/Users/billy/d1/xfile.txt', '/Users/billy/d2/xfile.txt')
