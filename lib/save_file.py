import json
from os import path
import shutil
import logging
from lib.game_object import GameObject
from datetime import datetime
from lib.common import *
from lib.proxy_dict import GameObjectJsonEncoder, ProxyDict


class SaveFile:

    SAVE_PATH = '~/.config/unity3d/noio/Kingdom'

    def __init__(self, filename=None):
        if not filename:
            filename = f'{SaveFile.SAVE_PATH}/storage_v34_AUTO.dat'

        self.path = path.expanduser(filename)

        if not path.exists(self.path):
            raise FileNotFoundError(f'Savefile not found at {self.path}')

        logging.debug(f'Savefile found at {self.path}')

        with open(self.path) as f:
            self._data = json.loads(f.read())

        self.objects = [GameObject(obj) for obj in self._data['objects']]
        self.player_preview = ProxyDict(self._data['playerPreview'])

    def add_object(self, obj):
        guard_type(obj, GameObject)
        self.objects += [obj]

    def remove_object_by_prefab(self, prefab):
        """
        Remove all objects with a prefabPath beginning with a specific value

        Arguments:
            id {string} -- prefabPath to match on

        Returns:
            int -- number of matching objects removed
        """
        guard_type(prefab, str)

        matches = [
            obj for obj in self.objects if obj.prefabPath.startswith(prefab)]
        if not matches:
            logging.warning(f'No matches found for {prefab}')
            return 0
        logging.info(f'Removing {len(matches)} instances of {prefab}')
        self.objects -= matches
        return len(matches)

    def remove_object_by_id(self, id):
        """
        Remove all objects with a unique ID beginning with a specific value

        Arguments:
            id {string} -- unique ID to match on

        Returns:
            int -- number of matching objects removed
        """
        guard_type(id, str)

        matches = [
            obj for obj in self.objects if obj.uniqueId.startswith(id)]
        if not matches:
            logging.warning(f'No matches found for {id}')
            return 0
        logging.info(f'Removing {len(matches)} instances of {id}')
        self.objects -= matches
        return len(matches)

    def write(self):
        self._data['objects'] = [obj.__dict__ for obj in self.objects]
        self._data['playerPreview'] = self.player_preview.__dict__
        with open(self.path, 'w') as f:
            f.write(json.dumps(self._data, cls=GameObjectJsonEncoder))
        logging.info(f'Savefile written to {self.path}')

    def backup(self, folder=None):

        if not folder:
            folder = SaveFile.SAVE_PATH

        timestamp = datetime.now().strftime('%Y%m%d-%H%M')
        filename = path.join(folder, f'storage_v34_AUTO.{timestamp}.dat.json')
        logging.info(f'Writing backup to {filename}')

        with open(path.expanduser(filename), 'w') as f:
            f.write(json.dumps(self._data, cls=GameObjectJsonEncoder))

    def restore(self, filename):
        if not path.exists(filename):
            raise ValueError(
                f'No backup file found at "{filename}" to restore')

        shutil.copyfile(filename, self.path)
