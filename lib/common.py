def guard_type(obj, expected_type):
    if not isinstance(obj, expected_type):
        raise TypeError(
            f'Expected "{type.__name__}" value; got "{type(obj).__name__}" value')
