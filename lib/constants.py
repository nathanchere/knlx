import lib.prefab as Prefab

characters = {
    'archer': Prefab.CHARACTER_ARCHER,
    'beggar': Prefab.CHARACTER_BEGGAR,
    'knight': Prefab.CHARACTER_KNIGHT,
    'merchant': Prefab.CHARACTER_MERCHANT,
    'peasant': Prefab.CHARACTER_PEASANT,
    'worker': Prefab.CHARACTER_WORKER,
    # Banker
}

steeds = {
    'horse': Prefab.STEED_HORSE_REGULAR,
    'warhorse': Prefab.STEED_WARHORSE,
    'unicorn': Prefab.STEED_UNICORN,
    'spookyhorse': Prefab.STEED_SPOOKY_HORSE,
    'stag': Prefab.STEED_STAG,
    'reindeer': Prefab.STEED_REINDEER,
    'bear': Prefab.STEED_BEAR,
}
