import collections
from os import path
import json
import logging
from lib.config import template_path
from lib.common import *
import lib.prefab as Prefab
from lib.proxy_dict import ProxyDict, GameObjectJsonEncoder
from random import random


class GameObject(ProxyDict):

    def __init__(self, obj):
        guard_type(obj, dict)

        # Just some stubs to remove linting errors
        self.uniqueID = None
        self.name = None
        self.localPosition = {}
        self.componentData2 = {}
        self.prefabPath = None
        self.parentObject = {}

        self.from_dict(obj)

    def __repr__(self):
        return f'GameObject<{self.uniqueID} ({self.prefabPath})>'

    def _is_type(self, prefab_path):
        return self.prefabPath.startswith(prefab_path)

    def is_building(self, prefab_path=Prefab.BUILDINGS):
        return self._is_type(prefab_path)

    def is_enemy(self, prefab_path=Prefab.ENEMIES):
        return self._is_type(prefab_path)

    def is_character(self, prefab_path=Prefab.CHARACTERS):
        return self._is_type(prefab_path)

    def is_steed(self, prefab_path=Prefab.STEEDS):
        return self._is_type(prefab_path)

    def is_player(self):
        return self._is_type(Prefab.CHARACTER_PLAYER)

    def is_monster(self):
        return (self.is_enemy(Prefab.ENEMY_BOSS) or
                self.is_enemy(Prefab.ENEMY_TROLL))

    def has_parent(self, parent_id):
        return self.parentObject.linkedObjectID == parent_id

    def randomise_id(self):
        id = int(random()*99999)
        self.uniqueID = self.name + f'--{id}'

    def set_position(self, x=None, y=None, z=None):
        if x:
            self.localPosition['x'] = x
        if y:
            self.localPosition['y'] = y
        if z:
            self.localPosition['z'] = z

    def set_invincible(self, is_invincible=None):
        """
        If no value set, toggles the existing value between true/false
        """
        for component in self.componentData2:
            if component['name'] == 'Damageable':
                node = json.loads(component['data'])
                result = not node['invulnerable'] if is_invincible is None else is_invincible
                node['invulnerable'] = result
                component['data'] = json.dumps(node)
        return result

    def to_json(self):
        return json.dumps(self, cls=GameObjectJsonEncoder)

    def dump(self, filename):
        filename = path.abspath(filename)
        print(f'Dumping object to {filename}')
        with open(filename, 'w') as f:
            f.write(self.to_json())

    @staticmethod
    def from_template(object_type):
        """
        Arguments:
            object_type {string} -- Expected in the format 'Prefabs/Category/ObjectName'
        """
        file_name = path.join(template_path, f'{object_type[8:]}.json')
        with open(file_name, 'r') as f:
            result = GameObject(json.loads(f.read()))
        result.randomise_id()

        return result
