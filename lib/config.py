from argparse import ArgumentParser, ArgumentTypeError
import logging
import sys
from os import path
import lib.prefab as Prefab
import lib.constants as constants


def argconv(**convs):
    def parse_argument(arg):
        if arg in convs:
            return convs[arg]
        else:
            msg = "invalid choice: {!r} (choose from {})"
            choices = ", ".join(sorted(repr(choice)
                                       for choice in convs.keys()))
            raise ArgumentTypeError(msg.format(arg, choices))
    return parse_argument


args = None
template_path = path.realpath(path.join(__file__, '../../templates'))


if len(sys.argv) > 1:
    parser = ArgumentParser()
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help="Display more logging")

    parser.add_argument(
        '--backup',
        type=str,
        nargs="?",
        const=True,
        metavar='filename',
        help="Create a backup copy of your current savegame")

    parser.add_argument(
        '--steed',
        choices=constants.steeds.keys(),
        help="Change the player's steed")

    parser.add_argument(
        '-k', '--kill',
        type=str,
        metavar='creatureType',
        nargs='+',
        help="Kill all of a specific creature type")

    parser.add_argument(
        '-a', '--armageddon',
        action='store_true',
        help="Kill (almost) all living things")

    parser.add_argument(
        '--build',
        action='store_true',
        help="Build (almost) all the things")

    parser.add_argument(
        '-u', '--upgrade',
        action='store_true',
        help="Upgrade buildings (TODO)")

    parser.add_argument(
        '-g', '--godmode',
        action='store_true',
        help="Toggle player invincibility (TODO)")

    parser.add_argument(
        '-m', '--money',
        action='store_true',
        help="Max out the player's money (TODO)")

    parser.add_argument(
        '--spawn',
        type=str,
        nargs='+',
        help="Spawn characters (TODO)")

    parser.add_argument(
        '--day',
        type=int,
        help="Set the current day (TODO)")

    parser.add_argument(
        '--boat',
        action='store_true',
        help="Complete the boat")

    args = parser.parse_args()
    logging_level = logging.DEBUG if args.verbose else logging.INFO
    args.__dict__['logging_level'] = logging_level
