ENEMIES = 'Prefabs/Enemies'
BUILDINGS = 'Prefabs/Buildings and Interactive'
CHARACTERS = 'Prefabs/Characters'
STEEDS = 'Prefabs/Steeds'

##############
# Enemies
##############

ENEMY_CLIFF_PORTAL = f'{ENEMIES}/Cliff Portal'
ENEMY_PORTAL = f'{ENEMIES}/Portal'
ENEMY_TROLL = f'{ENEMIES}/Troll'
ENEMY_BOSS = f'{ENEMIES}/Boss'

##############
# Buildings (castle)
##############

BUILDING_SHOP_HAMMER = f'{BUILDINGS}/ShopHammer'
BUILDING_SHOP_BOW = f'{BUILDINGS}/ShopBow'
# Shop - scythe
BUILDING_SHOP_CATAPULT = f'{BUILDINGS}/Workshop'

BUILDING_WALL = f'{BUILDINGS}/Wall'
BUILDING_WALL_0 = f'{BUILDING_WALL}0'
BUILDING_WALL_1 = f'{BUILDING_WALL}1'
BUILDING_WALL_2 = f'{BUILDING_WALL}2'
BUILDING_WALL_3 = f'{BUILDING_WALL}3'

BUILDING_TOWER = f'{BUILDINGS}/Tower'
BUILDING_TOWER_0 = f'{BUILDING_TOWER}0'
BUILDING_TOWER_1 = f'{BUILDING_TOWER}1'
BUILDING_TOWER_2 = f'{BUILDING_TOWER}2'
BUILDING_TOWER_3 = f'{BUILDING_TOWER}3'

BUILDING_FARMHOUSE = f'{BUILDINGS}/Farmhouse'
BUILDING_FARMHOUSE_0 = f'{BUILDING_FARMHOUSE}0'
# Farmhouse1

BUILDING_CASTLE = f'{BUILDINGS}/Castle'
BUILDING_CASTLE_0 = f'{BUILDING_CASTLE}0'
BUILDING_CASTLE_2 = f'{BUILDING_CASTLE}2'

##############
# Buildings (neutral)
##############

BUILDING_BEGGAR_CAMP = f'{BUILDINGS}/Beggar Camp'
BUILDING_STATUE_ARCHER = f'{BUILDINGS}/Statue Archer'
BUILDING_STATUE_TIME = f'{BUILDINGS}/Statue Time'
BUILDING_MASON_SHRINE_STONE = f'{BUILDINGS}/Mason Shrine Stone'

##############
# Characters
##############

CHARACTER_PLAYER = f'{CHARACTERS}/Player'

CHARACTER_ARCHER = f'{CHARACTERS}/Archer'
CHARACTER_BEGGAR = f'{CHARACTERS}/Beggar'
CHARACTER_KNIGHT = f'{CHARACTERS}/Knight'
CHARACTER_PEASANT = f'{CHARACTERS}/Peasant'
CHARACTER_WORKER = f'{CHARACTERS}/Worker'

# CHARACTER_BANKER = f'{CHARACTERS}/Banker'
CHARACTER_MERCHANT = f'{CHARACTERS}/Merchant'
CHARACTER_GHOST = f'{CHARACTERS}/Ghost'

##############
# Steeds
##############

STEED_HORSE_REGULAR = f'{STEEDS}/Horse Regular'
STEED_WARHORSE = f'{STEEDS}/Warhorse'
STEED_UNICORN = f'{STEEDS}/Unicorn'
STEED_STAG = f'{STEEDS}/Stag'
STEED_BEAR = f'{STEEDS}/Bear'
STEED_REINDEER = f'{STEEDS}/Reindeer'
STEED_SPOOKY_HORSE = f'{STEEDS}/SpookyHorse'
