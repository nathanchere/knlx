# Developer notes for savegame editor for Kingdom New Lands

## Property structure

### Common properties

* `name`
  - Strictly a label? Doesn't appear to have any other purpose.
* `parentObject`
  - ?
* `heirarchyPath`
  - See separate section about this (TODO)
* prefabPAth
  - See `Object heirarchy` section.
  - *Managers* seem to be the only things with no associated prefabs.


## prefabPath structure

Objects are grouped into categories as defined by their `prefabPath` property. The main groupings:

* `Prefabs/Triggers and Spawners/`
  * placeholders for the buildings which appear with each castle upgrade
* `Prefabs/Objects/`
  * Misc stuff like coins
* `Prefabs/Environment/`
  * Trees
* `Prefabs/Characters/`
  * The player
  * Workers
  * Ghost
* `Prefabs/Steeds/`
  * Anything the player can mount. Different horses, bear, unicorn etc.
* `Prefabs/Enemies/`
  * Monsters
  * Portals
* `Prefabs/Vegetation/`
  * Shrub birds?

Still to investigate exactly where to find:

* Ship (Wharf?)
* Wildlife (rabbits, deer)
* Banker
* Trees?

## heirarchyPath structure

TODO - investigate the implications of objects existing on the different layers

Examples:

* `Level/GameLayer/`
* `Managers/` - services / helpers for tracking things like achievements, enemy waves and overall game state

## Misc notes

Player positioned at the castle:

```
"localPosition": {
    "x": -3.3954873085021974,
    "y": 0.875,
    "z": 1.004472017288208
},
```

## Troubleshooting

Some objects are assigned to particular other objects. Make sure objects generated from templates don't point
to non-existent objects (e.g. an archer mounted in a tower reference which doesn't exist) or the whole savegave
will be wiped and regenerated on startup.

Archers can be assigned to a specific knight reference. Fix the archer template.

## TODO

Interesting things to explore

* Prefabs/Steeds
* Blossom trees (spawn unicorns?)
* PlayerPreview
  * what hats available?
  * what models?
  * 'skin' the character (e.g. alien) ?
* enemies - reset difficulty of next wave?
* director
  * clock speed
  * current day
* kingdom
  * coat of arms
  * what else?