Savegame editor for Kingdom New Lands

## Requirements

* Python >= 3.7
* Linux

Maybe works on Windows with a couple of minor tweaks but don't care. Probably works on OSX as-is but really don't care. 

## Installation

Clone repo wherever you like.

To make it easier to use: TODO symblink to usr bin

## Usage

###  Back-up save file

`knlx --backup [folder]`

Create a backup of the current game save file in the specified folder. If no folder specified, it saves to the same folder as the original game save file.

**Examples**:

- `knlx --backup`
- `knlx --backup ./backups`
- `knlx --backup ~/Documents/knlbak`


###  Change steed

`knlx --steed [steedType]`

Change the player's current steed to a different type. Type `knlx --help` to see available options.

**Examples**:

- `knlx --steed horse`
- `knlx --steed unicorn`
- `knlx --steed spookyhorse`


### Misc flags

* `-v`, `--verbose`: display more detailed/debugging info

-----

If you care about the internals or want to extend this further, I've left some [notes for developers](README.dev.md).